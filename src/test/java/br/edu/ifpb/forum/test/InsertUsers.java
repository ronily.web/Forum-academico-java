package br.edu.ifpb.forum.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.edu.ifpb.forum.dao.PersistenceUtil;
import br.edu.ifpb.forum.dao.RoleDAO;
import br.edu.ifpb.forum.dao.UserDAO;
import br.edu.ifpb.forum.entities.Role;
import br.edu.ifpb.forum.entities.User;

public class InsertUsers {
	private static EntityManagerFactory emf;
	private EntityManager em;

	@BeforeClass
	public static void init() {
		PersistenceUtil.createEntityManagerFactory("forum");
		emf = PersistenceUtil.getEntityManagerFactory();
//		ManagedEMContext.bind(emf, emf.createEntityManager());
		System.out.println("init()");
	}

	@AfterClass
	public static void destroy() {
		if (emf != null) {
			emf.close();
			System.out.println("destroy()");
		}
	}

	@Before
	public void initEM() {
		em = emf.createEntityManager();
	}
	
	@Test
	public void testInsertThemes() {
		try {
			System.out.println("entrando()");
			UserDAO dao = new UserDAO(em);
			RoleDAO roleDao = new RoleDAO(em);
			
			dao.beginTransaction();
			
			User adm = new User();
			User usr = new User();
			
			adm.setEmail("admin@gmail.com");
			adm.setCpf("00000000000");
			adm.setName("Administrador");
			adm.setPassword("admin");
			adm.setRole((Role)roleDao.find(1));
			dao.insert(adm);
			
			usr.setEmail("user1@gmail.com");
			usr.setCpf("11111111111");
			usr.setName("User_1");
			usr.setPassword("12345");
			usr.setRole((Role) roleDao.find(2));
			dao.insert(usr);
			
			// commitando objetos
			dao.commit();
		} catch (Exception e) {
			Assert.fail("Erro de BD: " + e);
		}
	}
}
