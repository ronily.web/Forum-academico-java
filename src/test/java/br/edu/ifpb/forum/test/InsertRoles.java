package br.edu.ifpb.forum.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.edu.ifpb.forum.dao.PersistenceUtil;
import br.edu.ifpb.forum.dao.RoleDAO;
import br.edu.ifpb.forum.entities.Role;

public class InsertRoles {
	private static EntityManagerFactory emf;
	private EntityManager em;

	@BeforeClass
	public static void init() {
		PersistenceUtil.createEntityManagerFactory("forum");
		emf = PersistenceUtil.getEntityManagerFactory();
//		ManagedEMContext.bind(emf, emf.createEntityManager());
		System.out.println("init()");
	}

	@AfterClass
	public static void destroy() {
		if (emf != null) {
			emf.close();
			System.out.println("destroy()");
		}
	}

	@Before
	public void initEM() {
		em = emf.createEntityManager();
	}
	
	@Test
	public void testInsertThemes() {
		try {
			System.out.println("entrando()");
			RoleDAO dao = new RoleDAO(em);
			
			dao.beginTransaction();
			
			Role adm= new Role();
			Role usr= new Role();
			
			adm.setRole("administrador");
			dao.insert(adm);
			
			usr.setRole("usuario");
			dao.insert(usr);
			
			
			// commitando objetos
			dao.commit();
		} catch (Exception e) {
			Assert.fail("Erro de BD: " + e);
		}
	}
}
