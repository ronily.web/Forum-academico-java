package br.edu.ifpb.forum.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.edu.ifpb.forum.dao.PersistenceUtil;
import br.edu.ifpb.forum.dao.ThemeDAO;
import br.edu.ifpb.forum.entities.Theme;

public class InsertThemes {
	private static EntityManagerFactory emf;
	private EntityManager em;

	@BeforeClass
	public static void init() {
		PersistenceUtil.createEntityManagerFactory("forum");
		emf = PersistenceUtil.getEntityManagerFactory();
//		ManagedEMContext.bind(emf, emf.createEntityManager());
		System.out.println("init()");
	}

	@AfterClass
	public static void destroy() {
		if (emf != null) {
			emf.close();
			System.out.println("destroy()");
		}
	}

	@Before
	public void initEM() {
		em = emf.createEntityManager();
	}
	
	@Test
	public void testInsertThemes() {
		try {
			System.out.println("entrando()");
			ThemeDAO dao = new ThemeDAO(em);
			
			// iniciando transa��o
			dao.beginTransaction();
			
			// tema 1
			Theme t1 = new Theme();
			t1.setTheme("ERRO");
			dao.insert(t1);
			
			// tema 2
			Theme t2 = new Theme();
			t2.setTheme("XP");
			dao.insert(t2);
			
			// tema 2
			Theme t3 = new Theme();
			t3.setTheme("RUP");
			dao.insert(t3);
			
			// commitando objetos
			dao.commit();
		} catch (Exception e) {
			Assert.fail("Erro de BD: " + e);
		}
	}
}
