package br.edu.ifpb.forum.dao;

import javax.persistence.EntityManager;

import br.edu.ifpb.forum.entities.Posting;
import br.edu.ifpb.forum.dao.GenericDAO;

public class PostingDAO extends GenericDAO<Posting, Integer>{
	public PostingDAO(EntityManager em) {
		super(em);
	}
}
