package br.edu.ifpb.forum.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifpb.forum.controller.PostingController;
import br.edu.ifpb.forum.controller.Result;
import br.edu.ifpb.forum.dao.PersistenceUtil;
import br.edu.ifpb.forum.entities.Posting;

public class ShowEditPostingCommand implements ICommand {
	
	@Override
	public Result execute(HttpServletRequest request, HttpServletResponse response) {
		String next = "edit_post.jsp"; 
		PostingController postingCtrl = new PostingController(PersistenceUtil.getCurrentEntityManager());

		Posting posting = postingCtrl.getPost(request.getParameter("id"));
		request.setAttribute("post", posting);
		
		Result result = new Result();
		result.setProximaPagina(next);
		return result;
	}

}
