package br.edu.ifpb.forum.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifpb.forum.controller.ThemeController;
import br.edu.ifpb.forum.controller.Result;
import br.edu.ifpb.forum.dao.PersistenceUtil;

public class StoreThemeCommand implements ICommand {
	
	@Override
	public Result execute(HttpServletRequest request, HttpServletResponse response) {
		String next = "index.jsp";
		ThemeController themeCtrl = new ThemeController(PersistenceUtil.getCurrentEntityManager());
		
		Result result = themeCtrl.store(request.getParameterMap());
		result.setProximaPagina(next);
		if (!result.isErro()) {
			result.setRedirect(true);
		} else {
			request.setAttribute("_msg", result.getMensagens());
			result.setProximaPagina(next);
		}
		
		return result;
	}

}
