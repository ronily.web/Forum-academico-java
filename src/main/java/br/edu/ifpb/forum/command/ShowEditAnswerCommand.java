package br.edu.ifpb.forum.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifpb.forum.controller.AnswerController;
import br.edu.ifpb.forum.controller.Result;
import br.edu.ifpb.forum.dao.PersistenceUtil;
import br.edu.ifpb.forum.entities.Answer;

public class ShowEditAnswerCommand implements ICommand {
	
	@Override
	public Result execute(HttpServletRequest request, HttpServletResponse response) {
		String next = "edit_answer.jsp"; 
		AnswerController answerCtrl = new AnswerController(PersistenceUtil.getCurrentEntityManager());

		Answer answer = answerCtrl.getAnswer(request.getParameter("id"));
		request.setAttribute("answer", answer);
		
		Result result = new Result();
		result.setProximaPagina(next);
		return result;
	}

}
