package br.edu.ifpb.forum.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifpb.forum.controller.PostingController;
import br.edu.ifpb.forum.controller.Result;
import br.edu.ifpb.forum.dao.PersistenceUtil;

public class EditPostingCommand implements ICommand {
	
	@Override
	public Result execute(HttpServletRequest request, HttpServletResponse response) {
		PostingController postingCtrl = new PostingController(PersistenceUtil.getCurrentEntityManager());
		
		String post_id = request.getParameter("id");
		String next = "controller.do?op=showPostingMessage&id=" + post_id;
		
		Result result = postingCtrl.edit(request.getParameterMap());
		result.setProximaPagina(next);
		if (!result.isErro()) {
			result.setRedirect(true);
		} else {
			request.setAttribute("_msg", result.getMensagens());
			next = "controller.do?op=showEditPosting&id=" + post_id;
			result.setProximaPagina(next);
		}
		
		return result;
	}

}
