package br.edu.ifpb.forum.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifpb.forum.controller.PostingController;
import br.edu.ifpb.forum.controller.Result;
import br.edu.ifpb.forum.dao.PersistenceUtil;
import br.edu.ifpb.forum.entities.User;

public class StorePostingCommand implements ICommand {
	
	@Override
	public Result execute(HttpServletRequest request, HttpServletResponse response) {
		PostingController postingCtrl = new PostingController(PersistenceUtil.getCurrentEntityManager());
		
		String next = "controller.do?op=showPostings&id=" + request.getParameter("theme_id");
		Result result = postingCtrl.store(request.getParameterMap(),(User) request.getSession().getAttribute("usuario"));
		
		result.setProximaPagina(next);
		if (!result.isErro()) {
			result.setRedirect(true);
		} else {
			request.setAttribute("_msg", result.getMensagens());
			result.setProximaPagina(next);
		}
		
		return result;
	}

}
