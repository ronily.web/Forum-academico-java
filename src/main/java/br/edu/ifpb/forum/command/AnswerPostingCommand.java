package br.edu.ifpb.forum.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifpb.forum.controller.AnswerController;
import br.edu.ifpb.forum.controller.Result;
import br.edu.ifpb.forum.dao.PersistenceUtil;
import br.edu.ifpb.forum.entities.User;


public class AnswerPostingCommand implements ICommand {
	
	@Override
	public Result execute(HttpServletRequest request, HttpServletResponse response) {
		AnswerController answerCtrl = new AnswerController(PersistenceUtil.getCurrentEntityManager());
		
		String next = "controller.do?op=showPostingMessage&id=" + request.getParameter("post_id");
		
		Result result = answerCtrl.store(request.getParameterMap(),(User) request.getSession().getAttribute("usuario"));
		result.setProximaPagina(next);
		if (!result.isErro()) {
			result.setRedirect(true);
		} else {
			request.setAttribute("_msg", result.getMensagens());
			result.setProximaPagina(next);
		}
		
		return result;
	}

}
