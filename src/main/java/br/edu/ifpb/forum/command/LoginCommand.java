package br.edu.ifpb.forum.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.edu.ifpb.forum.controller.LoginController;
import br.edu.ifpb.forum.controller.Result;
import br.edu.ifpb.forum.dao.PersistenceUtil;

public class LoginCommand implements ICommand{

	@Override
	public Result execute(HttpServletRequest request, HttpServletResponse response) {
		String next = "login.jsp";
		
		HttpSession sessao = request.getSession();
		
		LoginController loginCtrl = new LoginController(PersistenceUtil.getCurrentEntityManager());	
		
		Result r = loginCtrl.autentique(request.getParameterMap());
		
		if (!r.isErro()) {
			r.setRedirect(true);
			sessao.setAttribute("usuario", r.getEntidade());
			
		} else {

			request.setAttribute("_msg", r.getMensagens());
			r.setProximaPagina(next);
		}
		
		return r;
	}

}
