package br.edu.ifpb.forum.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifpb.forum.controller.Result;

public interface ICommand {
	
	Result execute(HttpServletRequest request, HttpServletResponse response);
	
}
