package br.edu.ifpb.forum.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.edu.ifpb.forum.controller.LoginController;
import br.edu.ifpb.forum.controller.Result;
import br.edu.ifpb.forum.controller.UserController;
import br.edu.ifpb.forum.dao.PersistenceUtil;

public class StoreUserCommand implements ICommand{

	@Override
	public Result execute(HttpServletRequest request, HttpServletResponse response) {
		String next = "index.jsp";
		String erro = "register.jsp";
		UserController ctrl = new UserController(PersistenceUtil.getCurrentEntityManager());
		LoginController ctrlLogin = new LoginController(PersistenceUtil.getCurrentEntityManager());
		
		HttpSession sessao = request.getSession();
		
		Result r = ctrl.store(request.getParameterMap());
		Result r2 = ctrlLogin.autentique(request.getParameterMap());
		
		System.out.println(r2.getEntidade());
		
		if (!r.isErro()) {
			r.setRedirect(true);
			sessao.setAttribute("usuario",r2.getEntidade());
			r.setProximaPagina(next);
			
		} else {
			request.setAttribute("_msg", r.getMensagens());
			r.setProximaPagina(erro);
		}
		
		return r;
	}

}
