package br.edu.ifpb.forum.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifpb.forum.controller.Result;

public class LogoutCommand implements ICommand{

	@Override
	public Result execute(HttpServletRequest request, HttpServletResponse response) {
		request.getSession().invalidate();
		Result resultado = new Result();
		resultado.setErro(false);
		resultado.setProximaPagina("index.jsp");
		resultado.setRedirect(true);
		return resultado;
	}
	
}
