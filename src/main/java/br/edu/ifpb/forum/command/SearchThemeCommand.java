package br.edu.ifpb.forum.command;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifpb.forum.controller.Result;
import br.edu.ifpb.forum.controller.ThemeController;
import br.edu.ifpb.forum.dao.PersistenceUtil;

public class SearchThemeCommand implements ICommand {
	
	@Override
	public Result execute(HttpServletRequest request, HttpServletResponse response) {
		ThemeController themeCtrl = new ThemeController(PersistenceUtil.getCurrentEntityManager());
		Map<String, String[]> parametros = request.getParameterMap();
		
		String filter = parametros.get("search")[0];
		String next = "index.jsp";
		
		System.out.println(themeCtrl.pesquisarPorFiltro(filter));
		
		request.setAttribute("search", filter);
		
		Result result = new Result();
		result.setProximaPagina(next);
		
		return result;
	}
}
