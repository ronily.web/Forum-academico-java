package br.edu.ifpb.forum.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.edu.ifpb.forum.controller.PostingController;
import br.edu.ifpb.forum.controller.Result;
import br.edu.ifpb.forum.dao.PersistenceUtil;
import br.edu.ifpb.forum.entities.Posting;

public class ShowPostingMessageCommand implements ICommand {
	
	@Override
	public Result execute(HttpServletRequest request, HttpServletResponse response) {
		String next = "post_message.jsp";
		PostingController postingCtrl = new PostingController(PersistenceUtil.getCurrentEntityManager());
	
		Posting posting = postingCtrl.getPost(request.getParameter("id"));
		HttpSession sessao = request.getSession();
		
		if(!sessao.isNew()){
			sessao.setAttribute("posting_user", posting.getUser());
		}
		
		request.setAttribute("post", posting);
		Result result = new Result();
		result.setProximaPagina(next);
		
		return result;
	}

}
