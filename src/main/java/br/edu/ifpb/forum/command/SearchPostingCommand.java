package br.edu.ifpb.forum.command;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifpb.forum.controller.PostingController;
import br.edu.ifpb.forum.controller.Result;
import br.edu.ifpb.forum.dao.PersistenceUtil;
import br.edu.ifpb.forum.dao.ThemeDAO;
import br.edu.ifpb.forum.entities.Posting;

public class SearchPostingCommand implements ICommand {
	
	@Override
	public Result execute(HttpServletRequest request, HttpServletResponse response) {
		PostingController postingCtrl = new PostingController(PersistenceUtil.getCurrentEntityManager());
	
		Map<String, String[]> parametros = request.getParameterMap();
		ThemeDAO themeDAO = new ThemeDAO(PersistenceUtil.getCurrentEntityManager());
		
		String theme_id = parametros.get("theme_id")[0];
		String next = "posts.jsp";
		
		request.setAttribute("theme", themeDAO.find(Integer.parseInt(theme_id)));
		request.setAttribute("posts", filter(postingCtrl.listarPorTema(theme_id), parametros.get("search")[0]));
		
		Result result = new Result();
		result.setProximaPagina(next);
		
		return result;
	}
	
	private List<Posting> filter(List<Posting> posts, String search) {
		List<Posting> result = new ArrayList<Posting>();
		String searchLowerTrimmed = search.toLowerCase().trim();
		for(Posting post: posts) {
			if(post.getTitle().toLowerCase().contains(searchLowerTrimmed) || 
					post.getMessage().toLowerCase().contains(searchLowerTrimmed))
				result.add(post);
		}
		return result;
	}
}
