package br.edu.ifpb.forum.command;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifpb.forum.controller.PostingController;
import br.edu.ifpb.forum.controller.Result;
import br.edu.ifpb.forum.dao.PersistenceUtil;
import br.edu.ifpb.forum.dao.ThemeDAO;

public class ShowPostingsCommand implements ICommand {
	
	@Override
	public Result execute(HttpServletRequest request, HttpServletResponse response) {
		String next = "posts.jsp";
		PostingController postingCtrl = new PostingController(PersistenceUtil.getCurrentEntityManager());
	
		Map<String, String[]> parametros = request.getParameterMap();
		ThemeDAO themeDAO = new ThemeDAO(PersistenceUtil.getCurrentEntityManager());
		
		String theme_id = parametros.get("id")[0];
		request.setAttribute("theme", themeDAO.find(Integer.parseInt(theme_id)));
		request.setAttribute("posts", postingCtrl.listarPorTema(theme_id));
		
		Result result = new Result();
		result.setProximaPagina(next);
		
		return result;
	}

}
