package br.edu.ifpb.forum.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifpb.forum.controller.PostingController;
import br.edu.ifpb.forum.controller.Result;
import br.edu.ifpb.forum.dao.PersistenceUtil;
import br.edu.ifpb.forum.entities.Posting;

public class RemovePostingCommand implements ICommand {
	
	@Override
	public Result execute(HttpServletRequest request, HttpServletResponse response) {
		PostingController postingCtrl = new PostingController(PersistenceUtil.getCurrentEntityManager());
		
		Posting posting = postingCtrl.getPost(request.getParameter("id"));
		
		String next = "controller.do?op=showPostings&id=" + posting.getTheme().getId();
		
		Result result = postingCtrl.remove(posting);
		result.setProximaPagina(next);
		if (!result.isErro()) {
			result.setRedirect(true);
		} else {
			request.setAttribute("_msg", result.getMensagens());
			result.setProximaPagina(next);
		}
		
		return result;
	}

}
