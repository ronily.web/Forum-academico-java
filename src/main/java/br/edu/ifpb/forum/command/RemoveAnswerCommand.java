package br.edu.ifpb.forum.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifpb.forum.controller.AnswerController;
import br.edu.ifpb.forum.controller.Result;
import br.edu.ifpb.forum.dao.PersistenceUtil;
import br.edu.ifpb.forum.entities.Answer;

public class RemoveAnswerCommand implements ICommand {
	
	@Override
	public Result execute(HttpServletRequest request, HttpServletResponse response) {
		AnswerController answerCtrl = new AnswerController(PersistenceUtil.getCurrentEntityManager());
	
		Answer answer = answerCtrl.getAnswer(request.getParameter("id"));
		
		String next = "controller.do?op=showPostingMessage&id=" + answer.getPost().getId();
		Result result = answerCtrl.remove(answer);
		result.setProximaPagina(next);
		if (!result.isErro()) {
			result.setRedirect(true);
		} else {
			request.setAttribute("_msg", result.getMensagens());
			result.setProximaPagina(next);
		}
		
		return result;
	}

}
