package br.edu.ifpb.forum.listener;

import java.io.IOException;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import br.edu.ifpb.forum.dao.PersistenceUtil;

@WebListener
public class ForumContextListener implements ServletContextListener {
	private EntityManagerFactory emf;

    public void contextDestroyed(ServletContextEvent e)  {
    	System.out.println("fechando sistema");
    	if (emf != null && emf.isOpen()) {
    		emf.close();
    	}
    }

    public void contextInitialized(ServletContextEvent e)  { 
         emf = PersistenceUtil.createEntityManagerFactory("forum");
         System.out.println("Fábrica de EntityManagers construída!");
         
         // Carrega o arquivo que mapeia operações em classes de comandos
         Properties p = new Properties();
 		try {
 			p.load(e.getServletContext().getResourceAsStream("/WEB-INF/commands.properties"));
 			e.getServletContext().setAttribute("commands", p);
 			System.out.println("Arquivo de comandos carregado!");
 		} catch (IOException e1) {
 			System.out.println("Erro ao carregar arquivo de comandos!");
 		}
         
    }
	
}
