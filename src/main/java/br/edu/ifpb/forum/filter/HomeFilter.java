package br.edu.ifpb.forum.filter;

import java.io.IOException;
import java.util.List;

import br.edu.ifpb.forum.dao.PersistenceUtil;
import br.edu.ifpb.forum.dao.ThemeDAO;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import org.hibernate.HibernateException;

import br.edu.ifpb.forum.entities.Theme;

@WebFilter(urlPatterns = { "/index.jsp" },dispatcherTypes={DispatcherType.REQUEST, DispatcherType.FORWARD})
public class HomeFilter implements Filter {

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		try {
			ThemeDAO dao = new ThemeDAO(PersistenceUtil.getCurrentEntityManager());
			String filter = (String) request.getAttribute("search");
			List<Theme> themes = (filter == null) ? dao.findAll() : dao.findByFilter(filter); 

			request.setAttribute("themes", themes);
		}
		catch(Exception e) {
			throw new HibernateException( "No entity manager currently bound to execution context" );
		}
		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

}
