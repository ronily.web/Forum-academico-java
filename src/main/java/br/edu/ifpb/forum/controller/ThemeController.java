package br.edu.ifpb.forum.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import br.edu.ifpb.forum.dao.ThemeDAO;
import br.edu.ifpb.forum.entities.Theme;

public class ThemeController {
	
	private EntityManager entityManager;
	
	private List<String> mensagensErro; 
	
	public ThemeController(EntityManager em) {
		this.entityManager = em;
	}

	public List<Theme> listar() {
		ThemeDAO dao = new ThemeDAO(entityManager);
		List<Theme> themes = dao.findAll();
		
		return themes;
	}

	public Result store(Map<String, String[]> parametros) {
		Result result = new Result();
		Theme theme = null;
		if ((theme = fromParametros(parametros)) != null) {
			ThemeDAO dao = new ThemeDAO(entityManager);
			System.out.println("theme controller");
			dao.beginTransaction();
			if (theme.getTheme() == null) {
				System.out.println("insert -> theme controller");
				dao.insert(theme);
			} else {
				System.out.println("update -> theme controller");
				dao.update(theme);
			}
			dao.commit();
			
			result.setErro(false);
			result.addMensagem("Tema criado com sucesso");
		} else {
			result.setEntidade(theme);
			result.setErro(true);
			result.setMensagens(this.mensagensErro);
		}
		
		return result;
	}

	private Theme fromParametros(Map<String, String[]> parametros) {
		String[] id = parametros.get("id");
		String[] nome = parametros.get("theme");
		
		Theme theme = new Theme();
		this.mensagensErro = new ArrayList<String>();
		
		if (id != null && id.length > 0 && !id[0].isEmpty()) {
			theme.setId(Integer.parseInt(id[0]));
		}
		
		if (nome == null || nome.length == 0 || nome[0].trim().isEmpty()) {
			this.mensagensErro.add("Nome obrigatório");
		} else {
			theme.setTheme(nome[0]);
		}
		
		return this.mensagensErro.isEmpty() ? theme : null ;
	}
	
	public List<Theme> pesquisarPorFiltro(String filter) {
		ThemeDAO dao = new ThemeDAO(entityManager);
		List<Theme> themes = dao.findByFilter(filter);
		
		return themes;
	}
}
