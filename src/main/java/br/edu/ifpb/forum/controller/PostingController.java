package br.edu.ifpb.forum.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import br.edu.ifpb.forum.dao.PostingDAO;
import br.edu.ifpb.forum.dao.ThemeDAO;
import br.edu.ifpb.forum.dao.UserDAO;
import br.edu.ifpb.forum.entities.Posting;
import br.edu.ifpb.forum.entities.Theme;
import br.edu.ifpb.forum.entities.User;

public class PostingController {
	
	private EntityManager entityManager;
	
	private List<String> mensagensErro; 
	
	public PostingController(EntityManager em) {
		this.entityManager = em;
	}

	public Posting getPost(String id) {
		int postingId = Integer.parseInt(id);
		PostingDAO dao = new PostingDAO(entityManager);
		return dao.find(postingId);
	}

	public List<Posting> listar() {
		PostingDAO dao = new PostingDAO(entityManager);
		List<Posting> postings = dao.findAll();
		
		return postings;
	}
	
	public List<Posting> listarPorTema(String theme_id) {
		PostingDAO dao = new PostingDAO(entityManager);
		List<Posting> postings = dao.findAllByColumn("theme_id", theme_id);
		return postings;
	}
	
	
	public Result store(Map<String, String[]> parameters, User user) {
		Result result = new Result();
		Posting posting = null;
		
		if (user == null){
			result.setErro(true);
			mensagensErro.add("Nenhum usuário identificado, faça login e tente novamente.");
			result.setMensagens(this.mensagensErro);
			return result;
		}
		
		if ((posting = storeFromParameters(parameters)) != null) {
			PostingDAO dao = new PostingDAO(entityManager);
			dao.beginTransaction();
			posting.setUser(user);
			dao.insert(posting);
			dao.commit();
			result.setErro(false);
			result.addMensagem("Postagem criada com sucesso!");
		} else {
			result.setEntidade(posting);
			result.setErro(true);
			result.setMensagens(this.mensagensErro);
		}
		
		return result;
	}

	private Posting storeFromParameters(Map<String, String[]> parameters) {
		String[] id = parameters.get("id");
		String[] title = parameters.get("title");
		String[] themeId = parameters.get("theme_id");
		String[] message = parameters.get("message");
		
		Posting posting = new Posting();
	
		this.mensagensErro = new ArrayList<String>();
		
		if (id != null && id.length > 0 && !id[0].isEmpty()) {
			posting.setId(Integer.parseInt(id[0]));
		}
		
		if (title == null || title.length == 0 || title[0].trim().isEmpty()) {
			this.mensagensErro.add("T�tulo obrigat�rio");
		} else {
			posting.setTitle(title[0]);
		}
		
		if (message == null || message.length == 0 || message[0].trim().isEmpty()) {
			this.mensagensErro.add("Conte�do obrigat�rio");
		} else {
			posting.setMessage(message[0]);
		}
		
		Date datePosting = new Date();
		posting.setDatePosting(datePosting);
		
		ThemeDAO themeDAO = new ThemeDAO(entityManager);
		int themeIdInt = Integer.parseInt(themeId[0]);
		Theme theme = themeDAO.find(themeIdInt);
		
		posting.setTheme(theme);
		
//		TODO Alterar para usu�rio real
		UserDAO userDAO = new UserDAO(entityManager);
		int userId = 1;		
		User user = userDAO.find(userId);
		
		posting.setUser(user);
		return this.mensagensErro.isEmpty() ? posting : null ;
	}
	
	public Result edit(Map<String, String[]> parameters) {
		Result result = new Result();
		Posting posting = null;
		if ((posting = editFromParameters(parameters)) != null) {
			PostingDAO dao = new PostingDAO(entityManager);
			dao.beginTransaction();
			dao.update(posting);
			dao.commit();
			result.setErro(false);
			result.addMensagem("Postagem editada!");
		} else {
			result.setEntidade(posting);
			result.setErro(true);
			result.setMensagens(this.mensagensErro);
		}
		
		return result;
	}
	
	public Posting editFromParameters(Map<String, String[]> parameters) {
		String[] id = parameters.get("id");
		String[] title = parameters.get("title");
		String[] message = parameters.get("message");
		
		Posting posting = this.getPost(id[0]);
		
		this.mensagensErro = new ArrayList<String>();
		
		if (title == null || title.length == 0 || title[0].trim().isEmpty()) {
			this.mensagensErro.add("T�tulo obrigat�rio");
		} else {
			posting.setTitle(title[0]);
		}
		
		if (message == null || message.length == 0 || message[0].trim().isEmpty()) {
			this.mensagensErro.add("Conte�do obrigat�rio");
		} else {
			posting.setMessage(message[0]);
		}
		
		return this.mensagensErro.isEmpty() ? posting : null ;
	}
	
	public Result remove(Posting posting) {
		Result result = new Result();
		PostingDAO dao = new PostingDAO(entityManager);
		dao.beginTransaction();
		dao.delete(posting);
		dao.commit();
		result.setErro(false);
		result.addMensagem("Postagem removida!");
		
		return result;
	}
}
