package br.edu.ifpb.forum.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import br.edu.ifpb.forum.dao.UserDAO;
import br.edu.ifpb.forum.entities.User;

public class LoginController {

	private EntityManager entityManager;
	private List<String> mensagensErro = null;

	public LoginController(EntityManager em) {
		this.entityManager = em;
		this.mensagensErro = new ArrayList<String>();
	}

	public void createMensage(String mensage) {
		if (mensage == null) return;
		this.mensagensErro.add(mensage);
	}

	public User findByEmail(Map<String, String[]> parametros) throws Exception{
		// Buscando parametros
		String[] pemail = parametros.get("email");
		String email = pemail[0];

		UserDAO dao = new UserDAO(this.entityManager);
		User user = null;

		dao.beginTransaction();
		// Buscando usuário no banco
		try {
			user = dao.buscaLogin(email);

		} catch (Exception e2) { // usuário não encontrado
			throw new Exception (e2.getMessage());
		}
		

		return (user != null) ? user : null;
	}

	public Result autentique(Map<String, String[]> parametros) {

		Result r = new Result();
		User user = null;
		
		try {
			user = findByEmail(parametros);
		} catch (Exception e) {
			createMensage(e.getMessage());
			r.addMensagens(mensagensErro);
			r.setErro(true);
			return r;
		}

		if (user.getEmail() != null && user.getPassword() != null) {

			String[] psenha = parametros.get("password");
			String senha = psenha[0];

			if (senha.equals(user.getPassword())) {
				r.setErro(false);
				r.setEntidade(user);
				r.setProximaPagina("index.jsp");
			} else {
				r.setErro(true);
				createMensage("Senha Incorreta");

			}

		} else {
			r.setErro(true);
			createMensage("Erro inesperado");

		}
		r.addMensagens(this.mensagensErro);
		return r;
	}

}
