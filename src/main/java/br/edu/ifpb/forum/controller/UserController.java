package br.edu.ifpb.forum.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import br.edu.ifpb.forum.dao.RoleDAO;
import br.edu.ifpb.forum.dao.UserDAO;
import br.edu.ifpb.forum.entities.Role;
import br.edu.ifpb.forum.entities.User;

public class UserController {
	private EntityManager entityManager;
	private List<String> mensagensErro = null;

	public UserController(EntityManager em) {
		this.entityManager = em;
		this.mensagensErro = new ArrayList<String>();
	}
	
	public void createMensage(String mensage) {
		if (mensage == null) return;
		this.mensagensErro.add(mensage);
	}
	
	public Result store (Map<String, String[]> parametros){
		User user = null;
		Result r = new Result();
		
		if ((user = fromParametros(parametros)) == null){
			r.setErro(true);
			r.addMensagens(mensagensErro);
			return r;
		}
		
		RoleDAO role = new RoleDAO(this.entityManager);
		UserDAO dao = new UserDAO(this.entityManager);
		dao.beginTransaction();
		try {
			user = dao.buscaLogin(user.getEmail());
			r.setErro(true);
			createMensage("Email já cadastrado.");
			r.addMensagens(mensagensErro);
			return r;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		try {
			user = dao.buscaCpf(user.getCpf());
			r.setErro(true);
			createMensage("CPF já cadastrado.");
			r.addMensagens(mensagensErro);
			return r;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		user.setRole((Role)role.find(2));
		
		dao.insert(user);
		dao.commit();
		
		r.addMensagem("Cadastrado com sucesso!");
		r.setErro(false);
		r.setEntidade(r);
		
		return r;
	}
	
	private User fromParametros(Map<String, String[]> parametros) {
		String cpf = parametros.get("cpf")[0];
		String nome = parametros.get("name")[0];
		String sobrenome = parametros.get("last_name")[0];
		String email = parametros.get("email")[0];
		String senha = parametros.get("password")[0];
		String confirmaSenha = parametros.get("password_confirmation")[0];
		
		User user = new User();
		
		//verificar tamanho cpf
		if(!validaCpf(cpf)){
			createMensage("cpf não confere.");
			return null;
		}
		cpf = cpf.replaceAll("[.-]", "");
		
		//verificar tamanho nome
		if(nome.length() < 3){
			createMensage("informe seu nome");
			return null;
		}
		
		// verificar se a senha foi informada e confirmada
		if(senha==null || confirmaSenha==null){
			createMensage("informe uma senha e confirme-a.");
			return null;	
		}
		
		//verificar se as senhas batem
		if(!senha.equals(confirmaSenha)){
			createMensage("senhas não batem!");
			return null;
		}
		
		user.setCpf(cpf);
		user.setEmail(email);
		user.setName(nome);
		user.setSobrenome(sobrenome);
		user.setPassword(senha);
		
		
		return user;
	}
	
	public boolean validaCpf (String cpf){
		
		String mascara = "^([0-9]{3}\\.?){3}-?[0-9]{2}$";
		boolean valida = cpf.matches(mascara);
		
		if(!valida){
			return false;
		}
		return true;
	}
}
