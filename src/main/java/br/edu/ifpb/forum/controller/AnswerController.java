package br.edu.ifpb.forum.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import br.edu.ifpb.forum.dao.AnswerDAO;
import br.edu.ifpb.forum.dao.PostingDAO;
import br.edu.ifpb.forum.dao.UserDAO;
import br.edu.ifpb.forum.entities.Answer;
import br.edu.ifpb.forum.entities.Posting;
import br.edu.ifpb.forum.entities.User;

public class AnswerController {
	
	private EntityManager entityManager;
	
	private List<String> mensagensErro; 
	
	public AnswerController(EntityManager em) {
		this.entityManager = em;
	}

	public Answer getAnswer(String answer_id) {
		int answerId = Integer.parseInt(answer_id);
		AnswerDAO dao = new AnswerDAO(entityManager);
		return dao.find(answerId);
	}

	public List<Answer> listar() {
		AnswerDAO dao = new AnswerDAO(entityManager);
		List<Answer> answers = dao.findAll();
		
		return answers;
	}
	
	public List<Answer> listarPorPost(String post_id) {
		AnswerDAO dao = new AnswerDAO(entityManager);
		List<Answer> answers = dao.findAllByColumn("post_id", post_id);
		return answers;
	}
	
	
	public Result store(Map<String, String[]> parameters, User user) {
		Result result = new Result();
		Answer answer = null;
		Posting posting = null;
		
		if(user == null){
			result.setEntidade(answer);
			result.setErro(true);
			this.mensagensErro.add("Apenas usuários logados podem responder!");
			result.setMensagens(this.mensagensErro);
			return result;
		}
		
		if ((answer = storeFromParameters(parameters)) != null) {
			PostingDAO postDAO = new PostingDAO(entityManager);
			AnswerDAO answerDAO = new AnswerDAO(entityManager);
			answer.setUser(user);
			posting = postDAO.find(Integer.parseInt(parameters.get("post_id")[0]));
			posting.addAnswer(answer);
			
			postDAO.beginTransaction();
			postDAO.insert(posting);
			postDAO.commit();
		
			answerDAO.beginTransaction();
			answerDAO.insert(answer);
			answerDAO.commit();
			
			result.setErro(false);
			result.addMensagem("Coment�rio criado com sucesso!");
		} else {
			result.setEntidade(answer);
			result.setErro(true);
			result.setMensagens(this.mensagensErro);
		}
		
		return result;
	}

	private Answer storeFromParameters(Map<String, String[]> parameters) {
		String[] id = parameters.get("id");
		String[] post_id = parameters.get("post_id");
		String[] message = parameters.get("message");
//		String[] user = parameters.get("user_id");
		
		Answer answer = new Answer();
	
		this.mensagensErro = new ArrayList<String>();
		
		if (id != null && id.length > 0 && !id[0].isEmpty()) {
			answer.setId(Integer.parseInt(id[0]));
		}
		
		if (message == null || message.length == 0 || message[0].trim().isEmpty()) {
			this.mensagensErro.add("Conte�do obrigat�rio");
		} else {
			answer.setMessage(message[0]);
		}
		
		Date dateAnswer = new Date();
		answer.setDateAnswer(dateAnswer);
		
		PostingDAO postDAO = new PostingDAO(entityManager);
		answer.setPost(postDAO.find(Integer.parseInt(post_id[0])));
		
//		TODO Alterar para usu�rio real
		UserDAO userDAO = new UserDAO(entityManager);
		int userId = 1;		
		User user = userDAO.find(userId);
		
		answer.setUser(user);
		return this.mensagensErro.isEmpty() ? answer : null ;
	}
	
	public Result edit(Map<String, String[]> parameters) {
		Result result = new Result();
		Answer answer = null;
		if ((answer = editFromParameters(parameters)) != null) {
			AnswerDAO dao = new AnswerDAO(entityManager);
			dao.beginTransaction();
			dao.update(answer);
			dao.commit();
			result.setErro(false);
			result.addMensagem("Coment�rio editado!");
		} else {
			result.setEntidade(answer);
			result.setErro(true);
			result.setMensagens(this.mensagensErro);
		}
		
		return result;
	}
	
	public Answer editFromParameters(Map<String, String[]> parameters) {
		String[] id = parameters.get("id");
		String[] message = parameters.get("message");
		
		Answer answer = this.getAnswer(id[0]);
		
		this.mensagensErro = new ArrayList<String>();

		if (message == null || message.length == 0 || message[0].trim().isEmpty()) {
			this.mensagensErro.add("Conte�do obrigat�rio");
		} else {
			answer.setMessage(message[0]);
		}
		
		return this.mensagensErro.isEmpty() ? answer : null ;
	}
	
	public Result remove(Answer answer) {
		Result result = new Result();
		AnswerDAO dao = new AnswerDAO(entityManager);
		dao.beginTransaction();
		dao.delete(answer);
		dao.commit();
		result.setErro(false);
		result.addMensagem("Coment�rio removido!");
		
		return result;
	}
}
