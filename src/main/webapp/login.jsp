<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="template/header.jsp" />


<div class="container">
	<div class="row">
		<div class="col-md-12">
			<!-- TODO - login -->
			<c:if test="${not empty _msg}">
				<c:forEach var="_m" items="${_msg}">
					<div class="alert alert-danger alert-dismissable">
						${_m}
						<button type="button" class="close" data-dismiss="alert">�</button>
					</div>
				</c:forEach>
			</c:if>
		</div>
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Login</div>

				<div class="panel-body">
					<form class="form-horizontal" method="POST" action="controller.do">


						<div
							class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<label for="email" class="col-md-4 control-label">E-Mail
								Address</label>

							<div class="col-md-6">
								<input id="email" type="email" style="margin-bottom:10px;" class="form-control" name="email" value="" required autofocus> 
								<input type="hidden" name = "op" value="login" id = "op">
							</div>
						</div>

						<div
							class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
							<label for="password" class="col-md-4 control-label">Password</label>

							<div class="col-md-6">
								<input id="password" type="password" style="margin-bottom:10px;" class="form-control" name="password" required>

							</div>
						</div>

						<div class="form-group">
							<div class="col-md-8 col-md-offset-4">
								<button type="submit" class="btn btn-primary">Login</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<c:import url="template/footer.jsp" />
<c:set var="endofconversation" value="yes" scope="request"/>
