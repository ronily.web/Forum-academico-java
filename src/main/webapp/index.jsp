<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:import url="template/header.jsp" />

<div class="container">
	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-body">
					<form method="GET"
						action="${pageContext.request.contextPath}/controller.do"
						class="form-horizontal">
						<input type="hidden" name="op" value="searchTheme">
						<div class="form-group">
							<div class="col-md-10">
								<input id="pesquisar" type="text"
									placeholder="Informe um tema ..." name="search" value=""
									class="form-control">
							</div>
							<button type="submit" class="btn btn-default">
								<i class="fa fa-search"></i>
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<!-- TODO - login -->
			<c:if test="${not empty _msg}">
				<c:forEach var="_m" items="${_msg}">
					<div class="alert alert-danger alert-dismissable">
						${_m}
						<button type="button" class="close" data-dismiss="alert">�</button>
					</div>
				</c:forEach>
			</c:if>

			<c:if test="${not empty usuario}">
				<c:if test="${usuario.role.role eq 'administrador'}">
					<div class="panel panel-default">
						<div class="panel-heading">Temas</div>
						<div class="panel-body">
							<button id='button-tema' type="submit" class="btn btn-primary">
								Cadastrar Tema</button>
						</div>
						
						
						
						
						<div id="cadastrar-tema">
							<div class="panel panel-default panel-commentary">
								<div class="panel-heading">Cadastrar Tema</div>
								<div class="panel-body panel-body-padding-bottom">
									<form class="form-horizontal" method="POST"
										action="${pageContext.request.contextPath}/controller.do">
										<input type="hidden" name="op" value="storeTheme">
										<div class="form-group">
											<label for="tema" class="col-md-1 control-label">T�tulo</label>
											<div class="col-md-3">
												<input id="tema" placeholder="Digite o t�tulo do tema aqui!"
													type="text" class="form-control" name="theme"
													value="${theme.theme}" autofocus>
											</div>
											<button type="submit" class="btn btn-primary">
												Cadastrar</button>
										</div>

									</form>
								</div>
							</div>
						</div>
					</div>
				</c:if>
			</c:if>
		</div>
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="list-group">
					<div class="panel-heading list-group-item-heading">
						<h4>Temas</h4>
					</div>
					<c:choose>
						<c:when test="${fn:length(themes) < 1}">
							<div class="panel-body panel-body-padding-bottom list-group-item">
								<p class="none">Nenhum tema foi encontrado!</p>
							</div>
						</c:when>
						<c:otherwise>
							<c:forEach var="theme" items="${themes}">
								<a
									href="${pageContext.request.contextPath}/controller.do?op=showPostings&id=${theme.id}"
									class="list-group-item">
									<h4>${theme.theme}</h4>
								</a>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
	<button class="button_top btn btn-primary">Topo</button>
</div>

<c:import url="template/footer.jsp" />
<c:set var="endofconversation" value="true" scope="request" />