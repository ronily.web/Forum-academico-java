<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:import url="template/header.jsp" />

<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-horizontal" method="GET" action="${pageContext.request.contextPath}/controller.do">
                    	<input type="hidden" name="op" value="searchPosting">
                    	<input type="hidden" name="theme_id" value="${theme.id}">
                        <div class="form-group">
                            <div class="col-md-10">
                                <input id="pesquisar" type="text" placeholder="O que voc� procura sobre ${theme.theme}?" class="form-control" name="search" value="">
                            </div>
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <!-- TODO - login -->
            <c:if test="${not empty _msg}">
				<c:forEach var="_m" items="${_msg}">
					<div class="alert alert-danger alert-dismissable">
						${_m}
	                    <button type="button" class="close" data-dismiss="alert">�</button>
	                </div>
				</c:forEach>
			</c:if>
			
			<c:if test="${not empty usuario}">
	            <div class="panel panel-default">
	                <div class="panel-heading">Postagens</div>
	                <div class="panel-body">
	                 <button id='button-postagem' type="submit" class="btn btn-primary">
	                     Criar Postagem
	                 </button>
	                </div>
	                <div id="cadastrar-postagem">
	                    <div class="panel panel-default panel-commentary">
	                        <div class="panel-heading">Cadastrar Postagem</div>
	                        <div class="panel-body panel-body-padding-bottom">
	                            <form class="form-horizontal" method="POST" action="${pageContext.request.contextPath}/controller.do">
	                                <input type="hidden" name="op" value="storePosting">
	                                <div class="form-group">
	                                    <label for="titulo" class="col-md-1 control-label">T�tulo</label>
	                                    <div class="col-md-3">
	                                        <input id="titulo" placeholder="O t�tulo da postagem vem aqui!" type="text" class="form-control" name="title" value="" required autofocus>
	                                    </div>
	                                </div>
	                                <div class="form-group">
	                                	<input type="hidden" name="theme_id" value ="${theme.id}"/>         
	                                </div>
									<div class="form-group">
										<label for="mensagem" class="col-md-1 control-label">Conte�do</label>
										<div class="col-md-11">
											<textarea id="mensagem" style="resize: none;" placeholder="O que deseja compartilhar sobre ${theme.theme}?"
												class="form-control" name="message" rows="6" cols="50" required autofocus>
	                                        </textarea>
										</div>
									</div>
									<div class="form-group commentary-form">
	                                	<div class=" modal-footer">
	                                    	<button type="submit" class="btn btn-primary">
	                                        	Cadastrar
	                                        </button>     	       
	                                    </div>    	        
	                               	</div>
	                            </form>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </c:if>  
        <div class="col-md-12">
			<div class="panel panel-default">
				<div class="list-group">
					<div class="panel-heading list-group-item-heading">
						<h4>Postagens sobre ${theme.theme}</h4>
					</div>
					<c:choose>
						<c:when test="${fn:length(posts) < 1}">
							<div class="panel-body panel-body-padding-bottom list-group-item">
								<p class="none">Nenhuma postagem foi encontrada!</p>
							</div>
						</c:when>
						<c:otherwise>
							<c:forEach var="post" items="${posts}">
								<a href="${pageContext.request.contextPath}/controller.do?op=showPostingMessage&id=${post.id}" class="list-group-item">
									<h4>${post.title}</h4>
								</a>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
    </div>
    <button class="button_top btn btn-primary">Topo</button>
</div>
	
<c:import url="template/footer.jsp" />
<c:set var="endofconversation" value="true" scope="request"/>