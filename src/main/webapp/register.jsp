<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="template/header.jsp" />

<div class="container">
	<div class="row">
		<c:if test="${not empty _msg}">
			<c:forEach var="_m" items="${_msg}">
				<div class="alert alert-danger alert-dismissable">
					${_m}
					<button type="button" class="close" data-dismiss="alert">�</button>
				</div>
			</c:forEach>
		</c:if>
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Register</div>

				<div class="panel-body">
					<form class="form-horizontal" method="POST"
						action="controller.do?op=storeUser" name="form">


						<div class="form-group">
							<label for="name" class="col-md-4 control-label">Name</label>

							<div class="col-md-6">
								<input id="name" type="text" maxlength="100"
									class="form-control" name="name" value="" required autofocus>

							</div>
						</div>

						<div class="form-group">
							<label for="last_name" class="col-md-4 control-label">Last
								Name</label>

							<div class="col-md-6">
								<input id="last_name" type="text" maxlength="100"
									class="form-control" name="last_name" value="">


							</div>
						</div>

						<div class="form-group">
							<label for="email" class="col-md-4 control-label">E-Mail
								Address</label>

							<div class="col-md-6">
								<input id="email" type="email" maxlength="100"
									class="form-control" name="email" value="" required>

							</div>
						</div>

						<div class="form-group">
							<label for="cpf" class="col-md-4 control-label">CPF</label>
							<div class="col-md-6">
								<input type="text" name="cpf" class="form-control cpf" maxlength="14"
								pattern="\d{3}\.\d{3}\.\d{3}-\d{2}" title="Digite o CPF no formato nnn.nnn.nnn-nn" 
								OnKeyPress="formatar('###.###.###-##', this)">


							</div>
						</div>

						<div class="form-group">
							<label for="password" class="col-md-4 control-label">Password</label>

							<div class="col-md-6">
								<input id="password" type="password" maxlength="60"
									class="form-control" name="password" required>

							</div>
						</div>

						<div class="form-group">
							<label for="password-confirm" class="col-md-4 control-label">Confirm
								Password</label>

							<div class="col-md-6">
								<input id="password_confirmation" type="password" maxlength="60"
									class="form-control" name="password_confirmation" required>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">Register
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	function formatar(mascara, documento){
	  var i = documento.value.length;
	  var saida = mascara.substring(0,1);
	  var texto = mascara.substring(i)
	  
	  if (texto.substring(0,1) != saida){
	            documento.value += texto.substring(0,1);
	  }
	  
	}
</script>
<c:import url="template/footer.jsp" />
<c:set var="endofconversation" value="true" scope="request" />