<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="template/header.jsp" />

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<!-- TODO - login -->
			<c:if test="${not empty _msg}">
				<c:forEach var="_m" items="${_msg}">
					<div class="alert alert-danger alert-dismissable">
						${_m}
						<button type="button" class="close" data-dismiss="alert">�</button>
					</div>
				</c:forEach>
			</c:if>
		</div>
		<div class="col-md-12">
			<div class="panel panel-default">
				<form class="form-horizontal" method="POST" action="${pageContext.request.contextPath}/controller.do">
					<input type="hidden" name="op" value="editPosting">
					<input type="hidden" name="id" value="${post.id}">
					<div class="panel-heading">
						<textarea id="titulo" placeholder="Defina o t�tulo da postagem aqui!" 
						style="resize: none;" class="form-control" name="title" rows="1"
							cols="20" required autofocus>${post.title}</textarea>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<div class="col-md-12">
								<textarea id="mensagem" style="resize: none;" placeholder="Defina o conte�do de sua postagem aqui!"
									class="form-control" name="message" rows="6" cols="50"
									required autofocus>${post.message}</textarea>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<a class="btn btn-default" style="margin-right: 1%" href="${pageContext.request.contextPath}/index.jsp">
							Voltar � p�gina principal
						</a> 
						<span class="pull-right">
							<button type="submit" class="btn btn-primary">
								Confirmar Altera��es
							</button>
						</span>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<button class="button_top btn btn-primary">Topo</button>
	
<c:import url="template/footer.jsp" />
<c:set var="endofconversation" value="true" scope="request"/>