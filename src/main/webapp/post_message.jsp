<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:import url="template/header.jsp" />

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<!-- TODO - login -->
			<c:if test="${not empty _msg}">
				<c:forEach var="_m" items="${_msg}">
					<div class="alert alert-danger alert-dismissable">
						${_m}
						<button type="button" class="close" data-dismiss="alert">�</button>
					</div>
				</c:forEach>
			</c:if>
		</div>
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="row">
						<h5 class="col-md-11">${post.title}</h5>
						<div class="col-md-1">
							<div class="row" style="margin-bottom: 0; margin-top: 15%">
								<ul class="col-md-6">
								</ul>
								<div class="col-md-6 dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"
										role="button" aria-expanded="false"> <i
										class="fas fa-bars"></i>
									</a>
									
									<c:if test="${not empty usuario && not empty posting_user}">
										<c:if test="${posting_user.email eq usuario.email || usuario.role.role eq 'administrador'}">
											<ul class="dropdown-menu">
												<li><a
													href="${pageContext.request.contextPath}/controller.do?op=showEditPosting&id=${post.id}">Editar</a>
												</li>
												<li><a
													href="${pageContext.request.contextPath}/controller.do?op=removePosting&id=${post.id}">Remover</a>
												</li>
											</ul>
										</c:if>
									</c:if>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="conteudo-postagem" class="panel-body">${post.message }</div>
				<div class="panel-footer">
					<strong> Autor: ${post.user.email} - ${post.theme.theme} -
						<fmt:formatDate value="${post.datePosting}" pattern="dd/MM/yyyy" />
					</strong>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading  commentary">Coment�rios</div>
				<div class="panel-body">
					<c:choose>
						<c:when test="${fn:length(post.answers) < 1}">
							<p class="none">Nenhum coment�rio cadastrado!</p>	
						</c:when>
						<c:otherwise>
							<c:forEach var="answer" items="${post.answers}">
								<div class="panel panel-default">
									<div class="panel-heading">
										<div class="row">
											<h5 class="col-md-11"></h5>
											<div class="col-md-1">
												<div class="row" style="margin-bottom: 0; margin-top: 15%">
													<ul class="col-md-6">
													</ul>
													<div class="col-md-6 dropdown">
														<a href="#" class="dropdown-toggle" data-toggle="dropdown"
															role="button" aria-expanded="false"> <i
															class="fas fa-bars"></i>
														</a>
														
														<c:if test="${answer.user.name eq usuario.name || usuario.role.role eq 'administrador'}">
															<ul class="dropdown-menu">
																	<li><a
																		href="${pageContext.request.contextPath}/controller.do?op=showEditAnswer&id=${answer.id}">Editar</a>
																	</li>
																	<li><a
																		href="${pageContext.request.contextPath}/controller.do?op=removeAnswer&id=${answer.id}">Remover</a>
																	</li>
											
															</ul>
														</c:if>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="panel-body">${answer.message}</div>
									<div class="panel-footer">
										<strong> Autor: ${answer.user.email} -
											${post.theme.theme} - <fmt:formatDate
												value="${answer.dateAnswer}" pattern="dd/MM/yyyy" />
										</strong>
									</div>
								</div>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
			<div id="comentar-postagem" class="panel panel-default"
				style="margin-top: 2%; margin-bottom: 2%">
				<div class="panel-heading">Comentar</div>
				<form class="form-horizontal commentary-form" method="POST" action="${pageContext.request.contextPath}/controller.do">
					<input type="hidden" name="op" value="answerPosting"> 
					<input type="hidden" name="post_id" value="${post.id}">
					<div class="form-group commentary-form">
						<div class="col-md-12">
							<div class="modal-body">
								<div class="form-group commentary-form">
									<div class="col-md-12">
										<textarea id="comentario" style="resize: none;"
											placeholder="O que deseja acrescentar � discuss�o?"
											class="form-control" name="message" rows="4" cols="50"
											required autofocus>	
										</textarea>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="submit" class="btn btn-primary">Comentar</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<button class="button_top btn btn-primary">Topo</button>
<c:import url="template/footer.jsp" />
<c:set var="endofconversation" value="true" scope="request" />