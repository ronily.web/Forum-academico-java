<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="../template/header.jsp" />

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-horizontal" method="GET" action="TODO">
                        <div class="form-group">
                            <div class="col-md-11">
                                <input id="pesquisar" type="text" class="form-control" name="pesquisar" value="" disabled>
                            </div>
                            <button type="submit" class="btn btn-primary">Pesquisar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <c:if test="${usuario.perfil empty}">
                <div class="panel panel-default">
                    <div class="panel-heading">Temas</div>
                    <div class="panel-body">
                        <c:if test="${usuario.perfil eq 'Admin'}">
                            <button id='button-tema' type="submit" class="btn btn-primary">
                                Cadastrar Tema
                            </button>
                        </c:if>
                    </div>
                    <div id="cadastrar-tema">
                        <div class="panel panel-default panel-commentary">
                            <div class="panel-heading">Cadastrar Tema</div>
                            <div class="panel-body panel-body-padding-bottom">
                                <form class="form-horizontal" method="POST" action="${pageContext.request.contextPath}/controller.do">
                                    <input type="hidden" name="op" value="addTheme">
                                    <div class="form-group">
                                        <label for="tema" class="col-md-1 control-label">Tema</label>
                                        <div class="col-md-3">
                                            <input id="tema" type="text" class="form-control" name="tema" value="" required autofocus>
                                        </div>
                                        <button type="submit" class="btn btn-primary">
                                            Cadastrar
                                        </button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </c:if>
        </div>
        <c:forEach var="tema" items="${temas}">
	        <div class="col-md-12">
	            <div class="panel panel-default">
	                <div class="container-fluid">
	                    <a class="btn btn-default col-md-12" href="${pageContext.request.contextPath}/controller.do?op=edtoper&id=${tema.id}" role="button">
	                        <p class="h3">${tema.theme}</p>
	                    </a>
	                </div>
	            </div>
	        </div>
        </c:forEach>
    </div>
    <button class="button_top btn btn-primary">Topo</button>
</div>
	
<c:import url="../template/footer.jsp" />
<c:set var="endofconversation" value="true" scope="request"/>